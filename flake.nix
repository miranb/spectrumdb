{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-parts.url = "github:hercules-ci/flake-parts";
    devenv = {
      url = "github:cachix/devenv";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.pre-commit-hooks.follows = "pre-commit-hooks";
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {flake-parts, ...}:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux"];
      imports = [
        inputs.devenv.flakeModule
        inputs.pre-commit-hooks.flakeModule
      ];

      perSystem = {pkgs, ...}: {
        formatter = pkgs.alejandra;
        pre-commit = {
          check.enable = true;
          settings.hooks = {
            alejandra.enable = true;
            statix.enable = true;
            deadnix.enable = true;

            # SQL formatting hook
            sleek = {
              enable = true;
              name = "SQL formatting";
              entry = "sleek";
              files = "^migrations/$";
              types = ["text" "sql"];
              language = "system";
            };
          };
        }; # end of pre-commit

        devenv.shells.default = let
          pg-dbname = "spectrumdb"; # NOTE: db name == username
          in rec {
          packages = with pkgs; [
            gitui
            sqlx-cli
            sleek
          ];

          services.postgres = {
            enable = true;
            package = pkgs.postgresql_15_jit;
            listen_addresses = "127.0.0.1";
            port = 5432;
            initialDatabases = [
              {name = pg-dbname;}
            ];
            initialScript = ''
              CREATE USER ${pg-dbname};
              ALTER DATABASE ${pg-dbname} OWNER TO ${pg-dbname};
            '';
          };

          env = rec {
            SPECTRUM_POSTGRES_USER = pg-dbname;
            SPECTRUM_POSTGRES_PASSWORD = "mysupersecretpassword";
            SPECTRUM_POSTGRES_DB = SPECTRUM_POSTGRES_USER;
            SPECTRUM_POSTGRES_CONTAINER_NAME = "spectrum-postgres-dev";
            SPECTRUM_POSTGRES_PORT = (builtins.toString services.postgres.port);

            DATABASE_URL = builtins.concatStringsSep "" [
              "postgres://"
              SPECTRUM_POSTGRES_USER
              ":"
              SPECTRUM_POSTGRES_PASSWORD
              "@"
              services.postgres.listen_addresses
              ":"
              SPECTRUM_POSTGRES_PORT
              "/"
              SPECTRUM_POSTGRES_DB
            ];
          };
        }; # end of devenv.shells.default
      }; # end of perSystem
    }; # end of flake-parts.lib.mkFlake
}
