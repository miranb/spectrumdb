CREATE SCHEMA spectrum;

---------------
-- languages --
---------------
CREATE DOMAIN spectrum.ieft_locale AS VARCHAR(35) DEFAULT '';
CREATE TABLE spectrum.languages (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    locale spectrum.ieft_locale NOT NULL UNIQUE
);

-------------
-- entries --
-------------
CREATE TABLE spectrum.entry_statuses (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    status VARCHAR(16) UNIQUE NOT NULL
);

CREATE TABLE spectrum.entry_types (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    type VARCHAR(16) UNIQUE NOT NULL
);

CREATE TABLE spectrum.entries (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    entry_type_id INT REFERENCES spectrum.entry_types(id) NOT NULL,
    start_time TIMESTAMPTZ,
    end_time TIMESTAMPTZ,
    entry_status_id INT
        REFERENCES spectrum.entry_statuses(id) NOT NULL
);

CREATE TABLE spectrum.entry_titles (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    entry_id INT REFERENCES spectrum.entries(id) NOT NULL,
    language_id INT REFERENCES spectrum.languages(id) NOT NULL,
    title TEXT NOT NULL
);

CREATE TABLE spectrum.entry_descriptions (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    entry_id INT REFERENCES spectrum.entries(id) NOT NULL,
    language_id INT REFERENCES spectrum.languages(id) NOT NULL,
    description TEXT NOT NULL,
    UNIQUE (entry_id, language_id)
);

CREATE TABLE spectrum.entry_relation_types (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    relation_type TEXT NOT NULL UNIQUE
);

CREATE TABLE spectrum.entry_relations (
    entry_1_id INT REFERENCES spectrum.entries(id) NOT NULL,
    entry_2_id INT REFERENCES spectrum.entries(id) NOT NULL,
    entry_relation_type_id INT 
        REFERENCES spectrum.entry_relation_types(id) NOT NULL,
    order_num INT,
    CHECK (entry_1_id != entry_2_id),
    PRIMARY KEY (entry_1_id, entry_2_id)
);

-----------
-- staff --
-----------
CREATE TABLE spectrum.staff_types (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    type TEXT NOT NULL UNIQUE
);

CREATE TABLE spectrum.staff (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    staff_type_id INT REFERENCES spectrum.staff_types(id) NOT NULL
);

CREATE TABLE spectrum.staff_info (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    staff_id INT REFERENCES spectrum.staff(id) NOT NULL,
    language_id INT REFERENCES spectrum.languages(id) NOT NULL,
    name TEXT NOT NULL,
    description TEXT,
    UNIQUE (staff_id, language_id)
);

---------------------------
-- entry-staff relations --
---------------------------
CREATE TABLE spectrum.roles (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    role TEXT NOT NULL UNIQUE
);

CREATE TABLE spectrum.entry_staff (
    entry_id INT REFERENCES spectrum.entries(id),
    staff_id INT REFERENCES spectrum.staff(id),
    role_id INT REFERENCES spectrum.roles(id),
    PRIMARY KEY (entry_id, staff_id, role_id)
);

--------------------------------------------
-- characters & entry-character relations --
--------------------------------------------
CREATE TABLE spectrum.characters (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY
);

CREATE TABLE spectrum.character_info (
    id INT GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    character_id INT REFERENCES spectrum.characters(id) NOT NULL,
    language_id INT REFERENCES spectrum.languages(id) NOT NULL,
    name TEXT NOT NULL,
    description TEXT,
    UNIQUE (character_id, language_id)
);

CREATE TABLE spectrum.entry_characters (
    character_id INT REFERENCES spectrum.characters(id) NOT NULL,
    entry_id INT REFERENCES spectrum.entries(id) NOT NULL,
    played_by_staff_id INT REFERENCES spectrum.staff(id),
    PRIMARY KEY (character_id, entry_id, played_by_staff_id)
);
