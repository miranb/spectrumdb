ALTER TABLE spectrum.entries
ALTER COLUMN start_time TYPE timestamp,
ALTER COLUMN end_time TYPE timestamp;
