--------------------------------------------
-- characters & entry-character relations --
--------------------------------------------
DROP TABLE spectrum.entry_characters;
DROP TABLE spectrum.character_info;
DROP TABLE spectrum.characters;

---------------------------
-- entry-staff relations --
---------------------------
DROP TABLE spectrum.entry_staff;
DROP TABLE spectrum.roles;

-----------
-- staff --
-----------
DROP TABLE spectrum.staff_info;
DROP TABLE spectrum.staff;
DROP TABLE spectrum.staff_types;

-------------
-- entries --
-------------
DROP TABLE spectrum.entry_relations;
DROP TABLE spectrum.entry_relation_types;
DROP TABLE spectrum.entry_descriptions;
DROP TABLE spectrum.entry_titles;
DROP TABLE spectrum.entries;
DROP TABLE spectrum.entry_types;
DROP TABLE spectrum.entry_statuses;

------------------------------
-- languages & text content --
------------------------------
DROP TABLE spectrum.languages;
DROP DOMAIN spectrum.ieft_locale;

DROP SCHEMA spectrum;
